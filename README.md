# AWS Port Knock #

A security best practice is to restrict access to management ports (SSH, RDP, etc) from white listed known IP addresses. Unfortunately with remote workers moving between office, home and their mobile devices this isn't always easy to do in practice. Consequently you might end up with 0.0.0.0/0 in the allow rules of your security groups.

Restricting access to management ports reduces the attack surface, eliminates noise from port-scanning and automated break-in attempts and provides an additional safety net for incorrectly configured services.

The AWS Port Knock utility makes it easy to restrict access to management ports in practice. Once setup, remote workers only need to visit a web page and enter their username and access code to gain access. Their current IP address will be automatically added to the white-list, and will be removed and replaced with their new IP address when they sign-in from another location.

## Steps for Installation ##

1. Upload AWSPortKnock-1.0.jar to S3 bucket
2. Create IAM role
3. Create DynamoDB table
4. Create Lambda function
4. Create DynamoDB table
5. Create API Gateway endpoint
6. Test the solution

### Upload AWSPortKnock-1.0.jar to S3 bucket ###

Upload the JAR file `AWSPortKnock-1.0.jar` into a S3 bucket. (It is over the 10M limit for direct upload into Lambda.)

### Create IAM Role ###

Create an IAM Role using the AWS Console called `AWSPortKnock` as an AWS Service Role for AWS Lambda.

Attach the following inline policy, substituting `1234567890` with your account number without dashes and `sg-1111111` / `sg-22222222` with the security group(s) you want to control. Remember to update the regions listed accordingly as well!

    {
     "Version": "2012-10-17",
     "Statement": [
        {
            "Sid": "AllowCloudWatchLogCreation",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ]
        },
        {
            "Sid": "AllowDynamoDBTableAccess",
            "Effect": "Allow",
            "Action": [
                "dynamodb:GetItem",
                "dynamodb:PutItem"
            ],
            "Resource": [
                "arn:aws:dynamodb:eu-west-1:1234567890:table/AWSPortKnock"
            ]
        },
        {
            "Sid": "AllowSecurityGroupAccess",
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:RevokeSecurityGroupIngress"
            ],
            "Resource": [
                "arn:aws:ec2:eu-west-1:1234567890:security-group/sg-11111111",
                "arn:aws:ec2:ap-southeast-2:1234567890:security-group/sg-22222222",
            ]
        }
     ]
    }

### Create DynamoDB Table ###

In the AWS Console create a new DynamoDB table with the following settings:

* **Name:** AWSPortKnock
* **Primary Partition Key:** username
* **Read Capacity Units:** 5
* **Write Capacity Units:** 5

Create a test account in this table as follows:

    {
      "username": "john.smith",
      "accessCode": "5ce4441b-af36-45cf-b3fc-7378775361a8"
    }

Note that the `accessCode` should be a type 4 random UUID [generated here for example](https://www.uuidgenerator.net/). This is the same way that you can add new users to the system.

**IMPORTANT** Make sure you change the username and accessCode given above to something random and secure!

### Create Lambda Function ###

In the AWS Console create a new Lambda function with the following settings:

* **Name:** AWSPortKnock
* **Runtime:** Java 8
* **Upload from S3:** (specify the URL to the file uploaded earlier)
* **Handler:** org.bitbucket.gavannewell.awsportknock.PortKnockFunction
* **Role:** AWSPortKnock
* **Memory:** 192MB
* **Timeout:** 30 seconds

Create test data for this function as follows:

    {
      "username": "john.smith",
      "accessCode": "5ce4441b-af36-45cf-b3fc-7378775361a8",
      "location": "Home",
      "ipAddress": "143.1.2.3",
      "userAgent": "Mozilla/5.0",
            "settings" : {
                "dynamoDbTableName": "AWSPortKnock",
                "dynamoDbTableRegion": "eu-west-1",
                "securityGroups": "eu-west-1:sg-11111111;ap-southeast-2:sg-22222222",
                "tcpManagementPorts": "22,3389",
                "udpManagementPorts": "5000"
          }
    }

### 5. Create API Gateway Endpoint ###

In the AWS Console create a new API Gateway as follows:

* **Name:** AWSPortKnock

Create a POST method that integrates with our Lambda function. Leave the models as Empty by default.

For the POST method Integration Request set the Request Body Passthrough to `Never` to ensure that the client IP address and settings cannot be faked. 

Create the following mapping for the `application/json` content type:

    #set($inputRoot = $input.path('$'))
    {
      "username" : "$inputRoot.username",
      "accessCode" : "$inputRoot.accessCode",
      "location" : "$inputRoot.location",
      "ipAddress" : "$context.identity.sourceIp",
      "userAgent" : "method.request.header.User-Agent",
      "settings" : {
            "dynamoDbTableName": "${stageVariables.dynamoDbTableName}",
            "dynamoDbTableRegion": "${stageVariables.dynamoDbTableRegion}",
            "securityGroups": "${stageVariables.securityGroups}",
            "tcpManagementPorts": "$!{stageVariables.tcpManagementPorts}",
            "udpManagementPorts": "$!{stageVariables.udpManagementPorts}"
      }
    }

Create the following mapping for the `application/x-www-form-urlencoded` content type:

    ## convert HTML POST data or HTTP GET query string to JSON

    ## get the raw post data from the AWS built-in variable and give it a nicer name
    #if ($context.httpMethod == "POST")
     #set($rawAPIData = $input.path('$'))
    #elseif ($context.httpMethod == "GET")
     #set($rawAPIData = $input.params().querystring)
     #set($rawAPIData = $rawAPIData.toString())
     #set($rawAPIDataLength = $rawAPIData.length() - 1)
     #set($rawAPIData = $rawAPIData.substring(1, $rawAPIDataLength))
     #set($rawAPIData = $rawAPIData.replace(", ", "&"))
    #else
     #set($rawAPIData = "")
    #end

    ## first we get the number of "&" in the string, this tells us if there is more than one key value pair
    #set($countAmpersands = $rawAPIData.length() - $rawAPIData.replace("&", "").length())

    ## if there are no "&" at all then we have only one key value pair.
    ## we append an ampersand to the string so that we can tokenise it the same way as multiple kv pairs.
    ## the "empty" kv pair to the right of the ampersand will be ignored anyway.
    #if ($countAmpersands == 0)
     #set($rawPostData = $rawAPIData + "&")
    #end

    ## now we tokenise using the ampersand(s)
    #set($tokenisedAmpersand = $rawAPIData.split("&"))

    ## we set up a variable to hold the valid key value pairs
    #set($tokenisedEquals = [])

    ## now we set up a loop to find the valid key value pairs, which must contain only one "="
    #foreach( $kvPair in $tokenisedAmpersand )
     #set($countEquals = $kvPair.length() - $kvPair.replace("=", "").length())
     #if ($countEquals == 1)
      #set($kvTokenised = $kvPair.split("="))
      #if ($kvTokenised[0].length() > 0)
       ## we found a valid key value pair. add it to the list.
       #set($devNull = $tokenisedEquals.add($kvPair))
      #end
     #end
    #end

    ## next we set up our loop inside the output structure "{" and "}"
    {
      "ipAddress" : "$context.identity.sourceIp",
      "userAgent" : "method.request.header.User-Agent",
          "settings" : {
                "dynamoDbTableName": "${stageVariables.dynamoDbTableName}",
                "dynamoDbTableRegion": "${stageVariables.dynamoDbTableRegion}",
                "securityGroups": "${stageVariables.securityGroups}",
                "tcpManagementPorts": "$!{stageVariables.tcpManagementPorts}",
                "udpManagementPorts": "$!{stageVariables.udpManagementPorts}"
          },
    #foreach( $kvPair in $tokenisedEquals )
      ## finally we output the JSON for this pair and append a comma if this isn't the last pair
      #set($kvTokenised = $kvPair.split("="))
     "$util.urlDecode($kvTokenised[0])" : #if($kvTokenised[1].length() > 0)"$util.urlDecode($kvTokenised[1])"#{else}""#end#if( $foreach.hasNext ),#end
    #end
    }

In the Integration Response create a mapping for the `application/json` content type:

      #set($inputRoot = $input.path('$'))
      {
        "success" : "$inputRoot.success",
        "message" : "$inputRoot.message"
      }

Create a GET resource as a Mock Endpoint. Under the Integration Response change the content type to `text/html` and use the following mapping:

    <html>
    <head>
    <title>AWS Port Knock</title>
    </head>
    <body>
    <h1>AWS Port Knock</h1>
    <form method="post" action=".">
    <table border="0">
    <tr><td>Username:</td><td><input type="text" name="username" size="50"></td></tr>
    <tr><td>Access Code:</td><td><input type="password" name="accessCode" size="50"><br/></td></tr>
    <tr><td>Location:</td><td><select name="location" required="required">
    <option value="Home">Home</option>
    <option value="Mobile">Mobile</option>
    <option value="Office">Office</option>
    </select><br/></td></tr>
    <tr><td colspan="2"><center><input type="submit" value="Submit"/></center></td></tr>
    </table>
    </form>
    </body>
    </html>

Change the Response Model content type to `text/html`.

Enable CORS for the API.

Deploy the API to the `prod` stage.

Create the following stage variables (refer to configuration section below for values):

* dynamoDbTableName = AWSPortKnock
* dynamoDbTableRegion = eu-west-1
* securityGroups = eu-west-1:sg-11111111;ap-southeast-2:sg-22222222
* tcpManagementPorts = 22,3389
* udpManagementPorts = 4500,5000

Create a user-friendly Custom Domain Name with an empty base path for this API. (If you don't want to do this, update the `action` in the HTML form above to match the stage name.)

### 6. Test the Solution ###

Visit your API URL in a web browser and try entering different values. Remember to edit your DynamoDB table contents!

## Configuration Reference ##

The configuration is performed using stage variables named as follows:

* dynamoDbTableName
* dynamoDbTableRegion
* securityGroups
* tcpManagementPorts
* udpManagementPorts

The `dynamoDbTableName` is the name of the DynamoDB table.

The `dynamoDbTableRegion` is the name of a region such as `eu-west-1`.

The `securityGroups` is a comma delimited list of security groups prefixed by their region. For example `eu-west-1:sg-11111111;ap-southeast-2:sg-22222222`.

The `tcpManagementPorts` is an optional comma delimited list of TCP ports to open. If omitted then `udpManagementPorts` must be defined.

The `udpManagementPorts` is an optional comma delimited list of UDP ports to open. If omitted then `tcpManagementPorts` must be defined.