package org.bitbucket.gavannewell.awsportknock;

public class Constants {
    public static final String DYNAMODB_USERNAME_COLUMN = "username";
    public static final String DYNAMODB_ACCESS_CODE_COLUMN = "accessCode";
    public static final String DYNAMODB_LAST_LOGIN_COLUMN = "lastLogin";
    public static final String DYNAMODB_IP_ADDRESS_COLUMN = "ipAddress";
    public static final String DYNAMODB_LOCATION_COLUMN = "location";
}
