package org.bitbucket.gavannewell.awsportknock;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;

public class Settings {

    String dynamoDbTableName;
    String dynamoDbTableRegion;
    String tcpManagementPorts;
    String udpManagementPorts;
    String securityGroups;

    public Settings() {
    }

    public String getDynamoDbTableName() {
        return dynamoDbTableName;
    }

    public void setDynamoDbTableName(String dynamoDbTableName) {
        this.dynamoDbTableName = dynamoDbTableName;
    }

    public String getDynamoDbTableRegion() {
        return dynamoDbTableRegion;
    }

    Region getDynamoDbTableRegionAsRegion() {
        try {
            return RegionUtils.getRegion(dynamoDbTableRegion.trim().toLowerCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public void setDynamoDbTableRegion(String dynamoDbTableRegion) {
        this.dynamoDbTableRegion = dynamoDbTableRegion;
    }

    public String getTcpManagementPorts() {
        return tcpManagementPorts;
    }

    int[] getTcpManagementPortsAsArray() {
        return toIntArray(this.tcpManagementPorts);
    }

    private static int[] toIntArray(String toSplit) {
        if (toSplit.trim().isEmpty()) {
            return new int[]{};
        }

        String[] split = toSplit.split(",", -1);
        int[] results = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            results[i] = Integer.parseInt(split[i]);
        }
        return results;
    }

    public void setTcpManagementPorts(String tcpManagementPorts) {
        this.tcpManagementPorts = tcpManagementPorts;
    }

    public String getUdpManagementPorts() {
        return udpManagementPorts;
    }

    int[] getUdpManagementPortsAsArray() {
        return toIntArray(this.udpManagementPorts);
    }

    public void setUdpManagementPorts(String udpManagementPorts) {
        this.udpManagementPorts = udpManagementPorts;
    }

    public String getSecurityGroups() {
        return securityGroups;
    }

    Region[] getSecurityGroupsRegionArray() {
        String[] split = this.securityGroups.split(",", -1);
        Region[] result = new Region[split.length];
        for (int i = 0; i < split.length; i++) {
            String[] pair = split[i].split(":", 2);
            try {
                Region region = Region.getRegion(Regions.fromName(pair[0].trim().toLowerCase()));
//            String securityGroupId = pair[1];
                result[i] = region;
            } catch (IllegalArgumentException e) {
                result[i] = null;
            }
        }
        return result;
    }

    String[] getSecurityGroupsIdArray() {
        String[] split = this.securityGroups.split(",", -1);
        String[] result = new String[split.length];
        for (int i = 0; i < split.length; i++) {
            String[] pair = split[i].split(":", 2);
//            Region region = Region.getRegion(Regions.fromName(pair[0].trim().toLowerCase()));
            String securityGroupId = pair[1];
            result[i] = securityGroupId;
        }
        return result;
    }

    public void setSecurityGroups(String securityGroups) {
        this.securityGroups = securityGroups;
    }

}
