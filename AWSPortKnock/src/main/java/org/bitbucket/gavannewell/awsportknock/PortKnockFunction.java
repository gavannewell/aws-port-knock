package org.bitbucket.gavannewell.awsportknock;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context; 
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class PortKnockFunction implements RequestHandler<PortKnockRequest, PortKnockResponse> {
    
    private static AmazonDynamoDBClient dynamoDBclient = null;
    private static AmazonEC2Client ec2Client = null;
    
    public PortKnockFunction() {
    }

    @Override
    public PortKnockResponse handleRequest(PortKnockRequest request, Context context) {
        LambdaLogger log = context.getLogger();
        
        if( dynamoDBclient == null && request.settings != null && request.settings.getDynamoDbTableRegionAsRegion() != null) {
            dynamoDBclient = new AmazonDynamoDBClient();
            dynamoDBclient.setRegion(request.settings.getDynamoDbTableRegionAsRegion());
        }
        
        if( ec2Client == null ) {
            ec2Client = new AmazonEC2Client();
        }
        
        PortKnock portKnock = new PortKnock(request, log, dynamoDBclient, ec2Client);
        PortKnockResponse response = portKnock.portKnock();
        return response;
    }
}
