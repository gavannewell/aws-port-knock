package org.bitbucket.gavannewell.awsportknock;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.RevokeSecurityGroupIngressRequest;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import static org.bitbucket.gavannewell.awsportknock.Constants.DYNAMODB_ACCESS_CODE_COLUMN;
import static org.bitbucket.gavannewell.awsportknock.Constants.DYNAMODB_IP_ADDRESS_COLUMN;
import static org.bitbucket.gavannewell.awsportknock.Constants.DYNAMODB_LAST_LOGIN_COLUMN;
import static org.bitbucket.gavannewell.awsportknock.Constants.DYNAMODB_LOCATION_COLUMN;
import static org.bitbucket.gavannewell.awsportknock.Constants.DYNAMODB_USERNAME_COLUMN;

public class PortKnock {

    private final PortKnockRequest request;
    private final LambdaLogger logFunction;
    private final AmazonDynamoDBClient dynamoDBclient;
    private final AmazonEC2Client ec2Client;
    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public PortKnock(PortKnockRequest request, LambdaLogger logFunction,
            AmazonDynamoDBClient dynamoDBclient, AmazonEC2Client ec2Client) {
        this.request = request;
        this.logFunction = logFunction;
        this.dynamoDBclient = dynamoDBclient;
        this.ec2Client = ec2Client;
    }

    public PortKnockResponse portKnock() {

        try {

            this.assertConfiguredCorrectly(this.request.settings);

            String requestInvalidErrorMessage = this.validateRequestOrReturnErrorMessage();
            if (requestInvalidErrorMessage != null) {
                return new PortKnockResponse(requestInvalidErrorMessage, Boolean.FALSE);
            }

            this.logRequest();

            UserRecord user = this.findUsername(request.username);
            if (user == null) {
                logWarning("Username not found in DynamoDB");
                return new PortKnockResponse("Invalid username and/or access code", Boolean.FALSE);
            }

            if (!user.accessCode.trim().equalsIgnoreCase(request.accessCode.trim())) {
                logWarning("Incorrect access code provided");
                return new PortKnockResponse("Invalid username and/or access code", Boolean.FALSE);
            }

            this.revokeAccess(user);

            user.ipAddress = request.ipAddress;
            user.lastLogin = DATE_FORMAT.format(new Date());
            user.location = request.location;
            this.updateUserRecord(user);

            this.grantAccess(user);

            return new PortKnockResponse("Access granted from " + request.ipAddress, Boolean.TRUE);
        } catch (RuntimeException | Error e) {
            logError(e);
            throw e;
        }
    }

    private void assertConfiguredCorrectly(Settings settings) {
        if( settings == null ) throw new AssertionError("Invalid configuration; missing 'settings'");
        if( settings.dynamoDbTableName == null ) throw new AssertionError("Invalid configuration; missing 'settings.dynamoDbTableName'");
        if( settings.dynamoDbTableRegion == null ) throw new AssertionError("Invalid configuration; missing 'settings.dynamoDbTableRegion'");
        if( settings.securityGroups == null ) throw new AssertionError("Invalid configuration; missing 'settings.securityGroups'");
        if( settings.tcpManagementPorts == null ) throw new AssertionError("Invalid configuration; missing 'settings.tcpManagementPorts'");
        if( settings.udpManagementPorts == null ) throw new AssertionError("Invalid configuration; missing 'settings.udpManagementPorts'");
        
        final Region dynamoDbRegion = settings.getDynamoDbTableRegionAsRegion();
        final String[] securityGroupIds = settings.getSecurityGroupsIdArray();
        final Region[] securityGroupRegions = settings.getSecurityGroupsRegionArray();
        final int[] managementPortsTcp = settings.getTcpManagementPortsAsArray();
        final int[] managementPortsUdp = settings.getUdpManagementPortsAsArray();
        
        if( dynamoDbRegion == null ) 
        {
            printKnownRegions();
            throw new AssertionError("Invalid configuration; invalid region value for 'settings.dynamoDbTableRegion' of " + settings.dynamoDbTableRegion);
        }
        if( securityGroupIds.length == 0 ) throw new AssertionError("Invalid configuration; invalid value for 'settings.securityGroups' require at least one entry");
        if( securityGroupIds.length != securityGroupRegions.length ) throw new AssertionError("Invalid configuration; invalid value for 'settings.securityGroups' mismatch");
        if( managementPortsTcp.length == 0 && managementPortsUdp.length == 0 ) throw new AssertionError("Invalid configuration; must have at least one TCP or UDP port specified");
        
        for( Region region : securityGroupRegions ) {
            if( region == null ) {
                printKnownRegions();
                throw new AssertionError("Invalid configuration; invalid value for 'settings.securityGroups' unknown region");
            }
        }
    }
    
    private void printKnownRegions()
    {
        StringBuilder sb = new StringBuilder();
        for( Region region : RegionUtils.getRegions() )
            sb.append(region.getName()).append(" ");
        this.logInfo("Known regions include " + sb.toString());
    }

    private void logError(Throwable ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        String exceptionAsString = sw.toString();
        this.logFunction.log(DATE_FORMAT.format(new Date()) + "|ERROR|"
                + ex.getClass().getName() + "|" + ex.getMessage() + "|" + '\n'
                + exceptionAsString);
    }

    private void logWarning(String message) {
        this.logFunction.log(DATE_FORMAT.format(new Date()) + "|WARNING|" + message);
    }

    private void logInfo(String message) {
        this.logFunction.log(DATE_FORMAT.format(new Date()) + "|INFO|" + message);
    }

    private void logRequest() {
        this.logInfo("username=" + request.username + "|ipAddress=" + request.ipAddress + "|location=" + request.location + "\n|userAgent=" + request.userAgent);
    }

    private String validateRequestOrReturnErrorMessage() {
        request.username = trimToLength(request.username, 50).toLowerCase();
        request.accessCode = trimToLength(request.accessCode, 36);
        request.ipAddress = trimToLength(request.ipAddress, 16);
        request.location = trimToLength(request.location, 50);
        request.userAgent = trimToLength(request.userAgent, 100);

        if (request.username.isEmpty()) {
            return "Missing username";
        }
        if (request.accessCode.isEmpty()) {
            return "Missing accessCode";
        }
        if (request.ipAddress.isEmpty()) {
            return "Missing ipAddress";
        }
        if (request.location.isEmpty()) {
            return "Missing location";
        }
        if (request.userAgent.isEmpty()) {
            return "Missing userAgent";
        }

        boolean validIpAddress = false;
        try {
            InetAddress ipAddressInet = InetAddress.getByName(request.ipAddress);
            validIpAddress
                    = !ipAddressInet.isAnyLocalAddress()
                    && !ipAddressInet.isLinkLocalAddress()
                    && !ipAddressInet.isLoopbackAddress()
                    && !ipAddressInet.isMulticastAddress()
                    && !ipAddressInet.isSiteLocalAddress();
            request.ipAddress = ipAddressInet.toString().substring(1);
        } catch (UnknownHostException e) {
            // Ignore
        }
        boolean validAccessCode = false;
        try {
            UUID accessCodeUUID = UUID.fromString(request.accessCode);
            validAccessCode
                    = accessCodeUUID.variant() == 2
                    && accessCodeUUID.version() == 4;
            request.accessCode = accessCodeUUID.toString();
        } catch (IllegalArgumentException e) {
            // Ignore
        }

        if (!validIpAddress) {
            logWarning("Invalid IP address provided " + request.ipAddress);
            return "Invalid IP address";
        }
        if (!validAccessCode) {
            logWarning("Incorrectly formatted access code provided " + request.accessCode);
            return "Invalid username and/or access code";
        }

        request.ipAddress = request.ipAddress + "/32";

        return null;
    }

    private UserRecord findUsername(String username) {
        Map<String, AttributeValue> key = new HashMap<>();
        key.put(DYNAMODB_USERNAME_COLUMN, new AttributeValue(username));
        GetItemRequest getItemRequest = new GetItemRequest();
        getItemRequest.setTableName(this.request.settings.dynamoDbTableName);
        getItemRequest.withKey(key);
        GetItemResult getItemResult = dynamoDBclient.getItem(getItemRequest);

        if (getItemResult == null || getItemResult.getItem() == null || getItemResult.getItem().isEmpty()) {
            return null;
        }

        UserRecord userRecord = new UserRecord();
        AttributeValue emptyDefault = new AttributeValue("");
        userRecord.accessCode = getItemResult.getItem().getOrDefault(DYNAMODB_ACCESS_CODE_COLUMN, emptyDefault).getS();
        userRecord.ipAddress = getItemResult.getItem().getOrDefault(DYNAMODB_IP_ADDRESS_COLUMN, emptyDefault).getS();
        userRecord.lastLogin = getItemResult.getItem().getOrDefault(DYNAMODB_LAST_LOGIN_COLUMN, emptyDefault).getS();
        userRecord.location = getItemResult.getItem().getOrDefault(DYNAMODB_LOCATION_COLUMN, emptyDefault).getS();
        userRecord.username = getItemResult.getItem().getOrDefault(DYNAMODB_USERNAME_COLUMN, emptyDefault).getS();

        return userRecord;
    }

    private void revokeAccess(UserRecord userRecord) {
        if (userRecord.ipAddress.isEmpty()) {
            logInfo("No existing IP address to revoke access from");
            return;
        }
        
        final String[] securityGroupIds = this.request.settings.getSecurityGroupsIdArray();
        final Region[] securityGroupRegions = this.request.settings.getSecurityGroupsRegionArray();
        
        for( int i = 0; i < securityGroupIds.length; i++ ) {
            ec2Client.setRegion(securityGroupRegions[i]);
            logInfo("Attempting to revoke access from " + userRecord.ipAddress + " to " + securityGroupIds[i]);
            RevokeSecurityGroupIngressRequest revokeSecurityGroupIngressRequest = new RevokeSecurityGroupIngressRequest();
            revokeSecurityGroupIngressRequest.setGroupId(securityGroupIds[i]);
            revokeSecurityGroupIngressRequest.withIpPermissions(buildIpPermissions(userRecord.ipAddress));
            ec2Client.revokeSecurityGroupIngress(revokeSecurityGroupIngressRequest);
            logInfo("Revoked access from " + userRecord.ipAddress + " to " + securityGroupIds[i]);
        }
    }
    
    private IpPermission[] buildIpPermissions(String ipAddress) {
        
        final int[] tcpManagementPorts = this.request.settings.getTcpManagementPortsAsArray();
        final int[] udpManagementPorts = this.request.settings.getUdpManagementPortsAsArray();
        
        IpPermission[] ipPermissions = new IpPermission[tcpManagementPorts.length + udpManagementPorts.length];
        for( int i = 0; i < tcpManagementPorts.length; i++ ) {
            IpPermission ipPermission = new IpPermission();
            ipPermission.setFromPort(tcpManagementPorts[i]);
            ipPermission.setToPort(tcpManagementPorts[i]);
            ipPermission.setIpProtocol("tcp");
            ipPermission.withIpRanges(ipAddress);
            ipPermissions[i] = ipPermission;
        }
        for( int i = 0; i < udpManagementPorts.length; i++ ) {
            IpPermission ipPermission = new IpPermission();
            ipPermission.setFromPort(udpManagementPorts[i]);
            ipPermission.setToPort(udpManagementPorts[i]);
            ipPermission.setIpProtocol("udp");
            ipPermission.withIpRanges(ipAddress);
            ipPermissions[i+tcpManagementPorts.length] = ipPermission;
        }
        return ipPermissions;
    }

    private void updateUserRecord(UserRecord userRecord) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put(DYNAMODB_USERNAME_COLUMN, new AttributeValue(userRecord.username));
        item.put(DYNAMODB_ACCESS_CODE_COLUMN, new AttributeValue(userRecord.accessCode));
        item.put(DYNAMODB_LAST_LOGIN_COLUMN, new AttributeValue(userRecord.lastLogin));
        item.put(DYNAMODB_IP_ADDRESS_COLUMN, new AttributeValue(userRecord.ipAddress));
        item.put(DYNAMODB_LOCATION_COLUMN, new AttributeValue(userRecord.location));
        dynamoDBclient.putItem(this.request.settings.dynamoDbTableName, item);
        logInfo("Updated DynamoDB user record");
    }

    private void grantAccess(UserRecord userRecord) {
        final String[] securityGroupIds = this.request.settings.getSecurityGroupsIdArray();
        final Region[] securityGroupRegions = this.request.settings.getSecurityGroupsRegionArray();
        
        for( int i = 0; i < securityGroupIds.length; i++ ) {
            ec2Client.setRegion(securityGroupRegions[i]);
            logInfo("Attempting to grant access from " + userRecord.ipAddress + " to " + securityGroupIds[i]);
            AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest();
            authorizeSecurityGroupIngressRequest.setGroupId(securityGroupIds[i]);
            authorizeSecurityGroupIngressRequest.withIpPermissions(buildIpPermissions(userRecord.ipAddress));
            ec2Client.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
            logInfo("Granted access from " + userRecord.ipAddress + " to " + securityGroupIds[i]);
        }
    }

    private static String trimToLength(String input, int maxLength) {
        if (input == null) {
            input = "";
        }
        if (input.length() > maxLength) {
            input = input.substring(0, maxLength);
        }
        return input;
    }
}
