package org.bitbucket.gavannewell.awsportknock;

public class PortKnockResponse {
    Boolean success;
    String message;

    public PortKnockResponse() {
    }

    public PortKnockResponse(String message, Boolean success) {
        this.success = success;
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
